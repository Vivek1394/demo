import LoginForm from "./Components/LoginForm/LoginForm"
import ValidateOtp from "./Components/ValidateOtp/ValidateOtp"
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import "./App.css"

function App() {
  return (
    <div className="app">
      <Router>
        <Redirect to="/login" from="/" />
        <Switch>
          <Route path="/login" component={LoginForm} />
          <Route path="/validate/:id" component={ValidateOtp} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
