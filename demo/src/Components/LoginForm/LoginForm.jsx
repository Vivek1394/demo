import { useInput } from "../../CustomHooks/useInput";
import axios from 'axios';
import { useHistory } from "react-router-dom";
import SingleFieldForm from "../Form/SingleFieldForm";

export default function LoginForm() {

  const { value, bind, reset } = useInput('');
  const history = useHistory();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post("http://localhost:8010/proxy/core/otp/", { "phone_number": value });
      reset();
      history.push("/validate/" + res.data.id);
    } catch (e) {
      console.log(e)
    }
  }
  return <SingleFieldForm label1="Phone No." submitLabel="Submit" handleSubmit={handleSubmit} bind={bind} />;
}