import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import { useInput } from "../../CustomHooks/useInput";
import SingleFieldForm from "../Form/SingleFieldForm";

export default function ValidateOtp() {

  const { value, bind, reset } = useInput('');
  const { id } = useParams();
  const history = useHistory();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post("http://localhost:8010/proxy/core/validate/", {
        id,
        otp: "000000",
        "device_id": "React local server",
        "client_app_id": "c8adf501-268f-4dd2-a5eb-6c1b2d7a7b65"
      });

      console.log(res.data);
      console.log(value);
      reset();
      history.push("/login");
    } catch (e) {
      console.log(e);
    }
  }
  return <SingleFieldForm label1="Otp" submitLabel="Submit" handleSubmit={handleSubmit} bind={bind} />;
}