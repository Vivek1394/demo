import React from 'react'
import { cleanup, fireEvent, render } from "@testing-library/react"
import SingleFieldForm from './SingleFieldForm';

afterEach(cleanup);

describe("SingleFieldForm", () => {
  test("renders without crashing", () => {
    render(<SingleFieldForm />);
  });

  test("props are rendering", () => {
    const { getByText, getByTestId } = render(<SingleFieldForm label1="Hello" submitLabel="world" bind={{ value: "One", onChange: jest.fn() }} />);

    expect(getByText(/hello/i).textContent).toBe("Enter the Hello");
    expect(getByTestId("single-field-input").value).toBe("One");
    expect(getByText('world').value).toBe("world");

  })

  test("checking input fields behaviour", () => {
    const handleSubmit = jest.fn(e => e.preventDefault());
    const onChange = jest.fn();
    const { getByTestId, getByText } = render(<SingleFieldForm submitLabel="Submit" bind={{ onChange }} handleSubmit={handleSubmit} />);

    fireEvent.change(getByTestId("single-field-input"), { target: { value: "a" } });
    expect(onChange.mock.calls.length).toBe(1);
    fireEvent.change(getByTestId("single-field-input"), { target: { value: "ab" } });
    expect(onChange.mock.calls.length).toBe(2);

    expect(getByTestId("single-field-input").value).toBe("ab");

    fireEvent.click(getByText("Submit"));
    expect(handleSubmit.mock.calls.length).toBe(1);

  })


})