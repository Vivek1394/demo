export default function SingleFieldForm({ label1, submitLabel, handleSubmit, bind }) {
  return (
    <form onSubmit={handleSubmit} className="form">
      <label>
        <span>Enter the {label1}</span>
        <br />
        <input data-testid="single-field-input" className="field" type="text" {...bind} />
      </label>
      <br />
      <input type="submit" value={submitLabel} />
    </form>
  );
}